//
//  BorderButton.swift
//  Quick Stats
//
//  Created by mac on 08/03/21.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        
        layer.borderWidth = 1.5
        layer.borderColor = UIColor.white.cgColor
        
    }

}
