//
//  FavoriteCollectionCell.swift
//  Quick Stats
//
//  Created by mac on 20/03/21.
//

import UIKit

class FavoriteCollectionCell: UITableViewCell {

    @IBOutlet weak var favoriteCollectionNameLabel: UILabel!
    @IBOutlet weak var dateOfCreationLabel: UILabel!
    @IBOutlet weak var numbersCountLabel: UILabel!
    
    
    public static let identifier = "FavoriteCollectionCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func configure (with favoriteCollection : FavoriteCollection) {
        
        favoriteCollectionNameLabel.text = favoriteCollection.name
        dateOfCreationLabel.text = favoriteCollection.date
        numbersCountLabel.text = favoriteCollection.numbers?.count == 1 ? String("\(favoriteCollection.numbers?.count ?? 0) número") : String("\(favoriteCollection.numbers?.count ?? 0) números")
        
    }
    
    static func nibName () -> UINib {
        
        let nibName = UINib(nibName: "FavoriteCollectionCell", bundle: nil)
        
        return nibName
        
    }
    
    
}
