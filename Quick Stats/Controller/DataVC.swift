//
//  DataVC.swift
//  Quick Stats
//
//  Created by Carlos Hernández Vázquez on 08/03/21.
//

import UIKit

class DataVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: BorderButton!
    @IBOutlet weak var clearButton: BorderButton!
    @IBOutlet weak var calculateButton: BorderButton!
    
    //MARK: - Properties
    
    private var numbers : [Double] = []
    
    //MARK: - Lyfe Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        overrideUserInterfaceStyle = .light
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        enableCalculateButton()
    }
    

    //MARK: - Methods
    
    private func addAlertController (title : String, message : String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        return alert
        
    }
    
    private func enableCalculateButton () {
        
        calculateButton.isEnabled = numbers.count > 0 ? true : false
        
    }
    
    public static func configureCell (cell : UITableViewCell, number : Double) -> UITableViewCell {
        
        cell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.textLabel?.textColor = .white
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = String(describing: number)
        cell.textLabel?.font = UIFont(name: "Avenir Next Medium", size: 15)!
        
        return cell
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let resultsVC = segue.destination as? ResultsVC else { return }
        
        resultsVC.numbers = self.numbers
        
    }
    
    @objc func toggleMinus (textField : UITextField) {
        
        textField.toggleMinus()
        
    }
    
//MARK: - Actions

    @IBAction func addButtonWasPressed(_ sender: BorderButton) {
     
        var textField = UITextField()
        
        let addAlert : UIAlertController = addAlertController(title: "Añadir número", message: "Ingrese un número")
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        let addAction = UIAlertAction(title: "Añadir", style: .default) { (action) in
            
            if let number = textField.text {
                    
                if let numberDouble = Double(number) {
                    
                    self.numbers.append(numberDouble)
                    
                } else{
                    
                    let errorAlert = self.addAlertController(title: "Error", message: "El número ingresado es incorrecto");
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    errorAlert.addAction(action)
                    
                    self.present(errorAlert, animated: true, completion: nil)
                    
                }
                
                self.enableCalculateButton()
                self.tableView.reloadData()

            }
                        
        }
        
        addAlert.addTextField { (alertTextField) in
            
            self.toggleMinus(textField: textField)
            alertTextField.placeholder = "Ingrese un número"
            alertTextField.keyboardType = .decimalPad
            
            textField = alertTextField
            
        }
        
        addAlert.addAction(cancelAction)
        addAlert.addAction(addAction)
        
        present(addAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func clearButtonWasPressed(_ sender: BorderButton) {
        
        if numbers.count > 0 {
            
            let clearAlert = addAlertController(title: "Limpiar", message: "¿Estás seguro de limpiar todos los datos de tu tabla")
            
            let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            
            let yesAction = UIAlertAction(title: "Si", style: .default) { (action) in
                
                self.numbers.removeAll()
                self.enableCalculateButton()
                self.tableView.reloadData()
                
            }
            
            
            clearAlert.addAction(noAction)
            clearAlert.addAction(yesAction)
            
            present(clearAlert, animated: true, completion: nil)

            
        } else {
            
            let noNumbersAlert = addAlertController(title: "", message: "Aún no has ingresado números")
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            noNumbersAlert.addAction(action)
            
            present(noNumbersAlert, animated: true, completion: nil)
            
        }
        
    }
    
    
    @IBAction func calculateButtonWasPressed(_ sender: BorderButton) {
        
        if numbers.count >= 1 {
            
            calculateButton.isEnabled = true
            performSegue(withIdentifier: "toResults", sender: self)
            
        } else {
            
            calculateButton.isEnabled = false
            
        }
        
    }
    
}

//MARK: - UITableViewDelegate Methods

extension DataVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let removeAction = UIContextualAction(style: .destructive, title: "Borrar") { (contextualAction, view, boolValue) in
            
            self.numbers.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
        
        removeAction.backgroundColor = .black
        
        let editAction = UIContextualAction(style: .normal, title: "Editar") { (contextualAction, view, boolValue) in
        
            var editTextField = UITextField()
            
            let editAlert = self.addAlertController(title: "Editar", message: "Ingrese el nuevo número")
            
            let editAction = UIAlertAction(title: "Ingresar", style: .default) { (action) in
                
                if let newNumber = editTextField.text {
                    
                    if let newNumberDouble = Double(newNumber) {
                        
                        self.numbers[indexPath.row] = newNumberDouble
                        self.tableView.reloadData()
                        
                    } else {
                        
                        let errorAlert = self.addAlertController(title: "", message: "El número ingresado es incorrecto")
                        
                        let errorAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        
                        errorAlert.addAction(errorAction)
                        
                        self.present(errorAlert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            editAlert.addTextField { (alertTextField) in
                
                alertTextField.placeholder = "Ingrese el nuevo número"
                alertTextField.keyboardType = .decimalPad
                
                editTextField = alertTextField
                
            }
            
            //Add actions
            
            editAlert.addAction(editAction)
            editAlert.addAction(cancelAction)
            
            self.present(editAlert, animated: true, completion: nil)
            
        }
        
        editAction.backgroundColor = .darkGray
        
        let swipeActions = UISwipeActionsConfiguration(actions: [removeAction, editAction])
        
        return swipeActions
        
    }
    
}

//MARK: - UITableViewDataSource Methods

extension DataVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numbers.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell? {
            
            let number = numbers[indexPath.row]
            
            return cell.configureCell(for: cell, with: number)
            
        }
        
        return UITableViewCell()
        
    }
    
    
}

