//
//  NumbersReviewVC.swift
//  Quick Stats
//
//  Created by mac on 20/03/21.
//

import UIKit

class NumbersReviewVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var numbersTableView: UITableView!
    
    //MARK: - Properties
    
    public var numbers = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        numbersTableView.delegate = self
        numbersTableView.dataSource = self
        numbersTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        numbersTableView.backgroundColor = .black
        
    }
    
}


//MARK: - UITableViewDelegate Methods

extension NumbersReviewVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
        
            return "Números ingresados"
            
        } else {
            
            return ""
            
        }
    }
    
}

//MARK: - UITableViewDataSource Methods

extension NumbersReviewVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numbers.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = numbersTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? UITableViewCell {
            
            let number = numbers[indexPath.row]
            
            return cell.configureCell(for: cell, with: number)
            
        }
        
        
        return UITableViewCell()
        
    }
    
    
    
    
}
