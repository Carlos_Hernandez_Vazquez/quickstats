//
//  ResultsVC.swift
//  Quick Stats
//
//  Created by mac on 11/03/21.
//

import UIKit

class ResultsVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var collectionTitleLabels: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var meanLabel: UILabel!
    @IBOutlet weak var squaredSumLabel: UILabel!
    @IBOutlet weak var populationVarianceLabel: UILabel!
    @IBOutlet weak var standardDeviationLabel: UILabel!
    @IBOutlet weak var totalNumbersLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var sampleVarianceLabel: UILabel!
    @IBOutlet weak var sampleStandarDesviationLabel: UILabel!
    @IBOutlet weak var firstQuartilLabel: UILabel!
    @IBOutlet weak var medianLabel: UILabel!
    @IBOutlet weak var thirdQuartilLabel: UILabel!
    @IBOutlet weak var saveButton: BorderButton!
    @IBOutlet weak var numbersButton: UIButton!
    
    
    
    //MARK: - Properties

    public var collectionTitle : String?
    public var numbers : [Double] = []
    public var isSaved : Bool = false
    public var noNameKey : String = "noName"
    public var firstRunKey : String = "firstRun"
    
    
    //MARK: - Lyfe Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
        
    }
    
    //MARK: - Methods
    
    private func initView () {
        
        collectionTitleLabels.text = collectionTitle ?? "Resultados"
        
        saveButton.isHidden = isSaved ? true : false
        numbersButton.isHidden = isSaved ? false : true
        numbersButton.titleLabel?.textAlignment = .left
        
        let statisticsManager = StatisticManager(numbers: self.numbers)
        
        sumLabel.text = String("\(statisticsManager.getSum())")
        meanLabel.text = String("\(statisticsManager.getMean())")
        squaredSumLabel.text = String("\(statisticsManager.getSquareSum())")
        populationVarianceLabel.text = String("\(statisticsManager.getPopulationVariance())")
        standardDeviationLabel.text = String("\(statisticsManager.getStandardDeviation())")
        totalNumbersLabel.text = String("\(statisticsManager.getTotal())")
        minLabel.text = String("\(statisticsManager.getMin())")
        maxLabel.text = String("\(statisticsManager.getMax())")
        sampleVarianceLabel.text = String("\(statisticsManager.getSampleVariance())")
        sampleStandarDesviationLabel.text = String("\(statisticsManager.getSampleStandardDesviation())")
        firstQuartilLabel.text = String("\(statisticsManager.getFirstQuartil())")
        medianLabel.text = String("\(statisticsManager.getMedian())")
        thirdQuartilLabel.text = String("\(statisticsManager.getThirdQuartil())")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let numbersReviewVC = segue.destination as? NumbersReviewVC else { return }
        
        numbersReviewVC.numbers = self.numbers
        
        
    }
    
    
    //MARK: - Actions

    @IBAction func backButtonWasPressed(_ sender: BorderButton) {
        
        dismiss(animated: true, completion: nil)
        
        
    }
    
    @IBAction func saveButtonWasPressed(_ sender: BorderButton) {
        
        var favoriteCollectionName : String = ""
        let favoriteCollectionDate =  Date()
        let favoriteCollectionDateString = favoriteCollectionDate.addFormat()

        var textField = UITextField()
        
        let alert = UIAlertController(title: "Guardar", message: "Ingresa el nombre de tu muestra", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
        
        let saveAction = UIAlertAction(title: "Guardar", style: .default) { (action) in
            
            favoriteCollectionName = textField.text ?? "No name"
            
            if textField.text == "" {
                
                
                if !UserDefaults.standard.bool(forKey: self.firstRunKey) {
                    
                    UserDefaults.standard.setValue(true, forKey: self.firstRunKey)
                    UserDefaults.standard.set(1, forKey: self.noNameKey)
                    favoriteCollectionName = String("Muestra 1")
                
                } else {
                    
                    let noName = UserDefaults.standard.integer(forKey: self.noNameKey)
                    favoriteCollectionName = String("Muestra \(noName + 1)")
                    let newValue = UserDefaults.standard.integer(forKey: self.noNameKey) + 1
                    UserDefaults.standard.set(newValue, forKey: self.noNameKey)

                }
                
            }
            
            DataManager.instance.saveFavoriteCollection(name: favoriteCollectionName, numbers: self.numbers, date: favoriteCollectionDateString)
            
            self.saveButton.isHidden = true
            
        }

        alert.addTextField { (alertTextField) in
            
            alertTextField.placeholder = "Ingrese el nombre"
            alertTextField.keyboardType = .default
            
            textField = alertTextField
            
        }
        
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func viewNumbersWasPressed(_ sender: UIButton) {
        
        performSegue(withIdentifier: "goToProductsReview", sender: self)
        
    }
    
    

}
