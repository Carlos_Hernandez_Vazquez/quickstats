//
//  CollectionsVC.swift
//  Quick Stats
//
//  Created by mac on 10/03/21.
//

import UIKit
import CoreData

class CollectionsVC: UIViewController {

    @IBOutlet weak var favoriteCollectionsTableView: UITableView!
    @IBOutlet weak var favoriteCollectionsView: UIView!
    
    
    //MARK: - Properties
    
    private var favoriteCollections = [FavoriteCollection]()
    private var numbers : [Double]?
    private var collectionTitle : String?
    
    //MARK: - Lyfe Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoriteCollectionsTableView.delegate = self
        favoriteCollectionsTableView.dataSource = self
        favoriteCollectionsTableView.backgroundColor = .black
        favoriteCollectionsTableView.register(FavoriteCollectionCell.nibName(), forCellReuseIdentifier: FavoriteCollectionCell.identifier)
        favoriteCollectionsTableView.separatorStyle = .singleLine
        favoriteCollectionsTableView.separatorColor = .white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFavoriteCollections()
        checkFavorites()
        
    }
    
    //MARK: - Methods
    
    private func getFavoriteCollections () {
        
        favoriteCollections = DataManager.instance.fetchFavoriteCollections()
        favoriteCollectionsTableView.reloadData()

        
    }
    
    private func checkFavorites () {
        
        favoriteCollectionsView.isHidden = favoriteCollections.count >= 1 ? false : true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let resultsVC = segue.destination as? ResultsVC else { return }
        
        resultsVC.isSaved = true
        resultsVC.numbers = self.numbers ?? [Double]()
        resultsVC.collectionTitle = self.collectionTitle
        
    }
    

}



//MARK: - UITableViewDelegate Methods

extension CollectionsVC : UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        headerView.backgroundColor = .black
        let titleLabel = UILabel(frame: CGRect(x: view.frame.size.width / 2 - view.frame.width / 4, y: 0, width: view.frame.width, height: 40))
        titleLabel.text = "Coleciones guardadas"
        titleLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 18)!
        titleLabel.textColor = .white
        titleLabel.textAlignment = .left
        headerView.addSubview(titleLabel)
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let favoriteCollection = favoriteCollections[indexPath.row]
        let name = favoriteCollection.name ?? ""
        
        let deleteSwipeAction = UIContextualAction(style: .destructive, title: "Eliminar") { (action, view, boolValue) in
            
            let deleteAlert = UIAlertController(title: "Eliminar muestra", message: "¿Está seguro de borrar esta muestra? No se pueden borrar los cambios", preferredStyle: .alert)
            let deleteAction = UIAlertAction(title: "Borrar", style: .default) { (action) in
            
                DataManager.instance.deleteFavoriteCollection(name: name)
                self.favoriteCollections.remove(at: indexPath.row)
                self.favoriteCollectionsTableView.deleteRows(at: [indexPath], with: .left)
                self.checkFavorites()
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            deleteAlert.addAction(deleteAction)
            deleteAlert.addAction(cancelAction)
            
            self.present(deleteAlert, animated: true, completion: nil)
            
        }
        
        deleteSwipeAction.backgroundColor = .red
        deleteSwipeAction.image = UIImage(systemName: "trash")!
        
        
        let swipeActions = UISwipeActionsConfiguration(actions: [deleteSwipeAction])
        
        return swipeActions
        
    }
    
}

//MARK: - UITableViewDataSource

extension CollectionsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return favoriteCollections.count
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        favoriteCollectionsTableView.deselectRow(at: indexPath, animated: false)
        
        let favoriteCollection = favoriteCollections[indexPath.row]
        
        collectionTitle = favoriteCollection.name
        numbers = favoriteCollection.numbers
        
        print(indexPath.row)
        performSegue(withIdentifier: "toResultsFormCollections", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = favoriteCollectionsTableView.dequeueReusableCell(withIdentifier: FavoriteCollectionCell.identifier, for: indexPath) as? FavoriteCollectionCell {
            
            let favoriteCollection = favoriteCollections[indexPath.row]
            
            cell.configure(with: favoriteCollection)
            
            return cell
            
        }
        
        return FavoriteCollectionCell()
        
    }
    
}




