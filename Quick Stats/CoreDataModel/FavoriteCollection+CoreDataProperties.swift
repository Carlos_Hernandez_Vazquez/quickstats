//
//  FavoriteCollection+CoreDataProperties.swift
//  Quick Stats
//
//  Created by mac on 18/03/21.
//
//

import Foundation
import CoreData


extension FavoriteCollection {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FavoriteCollection> {
        return NSFetchRequest<FavoriteCollection>(entityName: "FavoriteCollection")
    }

    @NSManaged public var date: String?
    @NSManaged public var name: String?
    @NSManaged public var numbers: [Double]?

}

extension FavoriteCollection : Identifiable {

}
