//
//  DataManager.swift
//  Quick Stats
//
//  Created by mac on 18/03/21.
//

import UIKit
import CoreData

class DataManager : NSObject {
    
    static let instance = DataManager()
    
    private override init () { }
    
    //MARK: - Methods
    
    private func getContext () -> NSManagedObjectContext? {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        
        return appDelegate.persistentContainer.viewContext
    
    }
    
    public func saveFavoriteCollection (name : String, numbers : [Double], date : String) {
        
        guard let managedContext = getContext() else { return }
        
        let favoriteCollection = FavoriteCollection(context: managedContext)
        favoriteCollection.name = name
        favoriteCollection.numbers = numbers
        favoriteCollection.date = date
        save()
        
    }
    
    public func fetchFavoriteCollections () -> [FavoriteCollection] {
        
        guard let managedContext = getContext() else { return [FavoriteCollection]() }
        
        var favoritecollection = [FavoriteCollection]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoriteCollection")
        

        do {
            
            try favoritecollection = managedContext.fetch(request) as! [FavoriteCollection]
            debugPrint("Favorite Collection fetched")
            return favoritecollection
            
        } catch {
            debugPrint("Error Fetching data \(error.localizedDescription)")
            
        }
        
        return [FavoriteCollection]()
        
    }
    
    public func deleteFavoriteCollection (name : String) {
        
        guard let managedContext = getContext() else { return }
        
        let request : NSFetchRequest<FavoriteCollection> = FavoriteCollection.fetchRequest()
        
        request.predicate = NSPredicate(format: "name CONTAINS %@", name)
        
        var favoriteCollectionToDelete = [NSManagedObject]()
        
        do {
            
            favoriteCollectionToDelete = try managedContext.fetch(request)
            
        } catch {
            
            debugPrint("Error deleting \(error.localizedDescription)")
            
        }
        
        delete(favoriteCollectionToDelete: favoriteCollectionToDelete[0] as! FavoriteCollection)
        
    }
    
    
    private func save () {
        
        guard let managedContext = getContext() else { return }
        
        do {
            
            try managedContext.save()
            debugPrint("Context saved")
            
            
        } catch {
            
            debugPrint("Error saving context \(error.localizedDescription)")
        }
        
    }
    
    private func delete (favoriteCollectionToDelete favoriteCollection : FavoriteCollection) {
        
        guard let managedContext = getContext() else { return }
        
        managedContext.delete(favoriteCollection)
        save()
        
    }
    
}
