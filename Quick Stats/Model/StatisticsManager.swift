//
//  StatisticsManager.swift
//  Quick Stats
//
//  Created by Carlos Hernández Vázquez on 11/03/21.
//

import Foundation

struct StatisticManager {
    
    //Properties
    
    private var numbers : [Double]
    
    //Init
    
    init(numbers : [Double]) {
        
        self.numbers = numbers
        
    }
    
    //MARK: - Methods
        
    public func getSum () -> Double {
        
        return numbers.reduce(0, +)
        
    }
    
    public func getMean () -> Double {
        
        let sum = getSum()
        
        return  sum / Double(numbers.count)
        
    }
    
    public func getSquareSum () -> Double {
        
        let squaredNumbers = numbers.map { number in number * number }
        
        return squaredNumbers.reduce(0, +)
        
    }
    
    public func getPopulationVariance () -> Double{
        
        let totalNumbers = Double(numbers.count)
        let sum = getSumSquaredNumbersMinusMean()
        
        
        return sum / totalNumbers
        
    }
    
    public func getStandardDeviation () -> Double {
        
        let populationVariance = getPopulationVariance()
        
        return sqrt(populationVariance)
        
    }
    
    public func getTotal () -> Int {
        
        return numbers.count
        
    }
    
    public func getMin () -> Double {
        
        return numbers.min() ?? 0.0
        
    }
    
    public func getMax () -> Double {
        
        return numbers.max() ?? 0.0
        
    }
    
    public func getSampleVariance () -> Double {
        
        let sum = getSumSquaredNumbersMinusMean()
        
        return sum / Double(numbers.count - 1)
        
    }
    
    public func getSampleStandardDesviation () -> Double {
        
        let sampleVariance = getSampleVariance()
        
        return sqrt(sampleVariance)
        
    }
    
    private func getSumSquaredNumbersMinusMean () -> Double {
        
        let mean = getMean()
        var sum : Double = 0
        
        for number in numbers {
            
            let result = pow(number - mean, 2)
            sum += result
            
        }

        return sum
        
    }
    
    public func getFirstQuartil () -> Double {
        
        return getQuartil(quartilNumber: 1)
        
    }
    
    public func getMedian () -> Double{
        
        let sortedNumbers = sortNumbers()
            
        if sortedNumbers.count % 2 == 0 {
            
            return (sortedNumbers[sortedNumbers.count / 2 - 1] + sortedNumbers[sortedNumbers.count / 2]) / 2
            
            
        } else {
            
            return sortedNumbers[(sortedNumbers.count) / 2]
        }
        
    }
    
    
    public func getThirdQuartil () -> Double{
        
        return getQuartil(quartilNumber: 3)
        
    }
    
    private func getQuartil (quartilNumber : Int) -> Double {
        
        var quantilPosition : Double = 0.0
        
        if quartilNumber == 1 {
            
            quantilPosition = (Double(numbers.count) + 1) / 4
            print(quantilPosition)
            
             return getQuartilValue(quantilPosition)
            
        } else if quartilNumber == 3 {
            
            quantilPosition = (3 * (Double(numbers.count) + 1)) / 4
            print(quantilPosition)
            
            return getQuartilValue(quantilPosition)
            
        }
        
        return 0.0
        
    }
    
    private func getQuartilValue (_ result : Double) -> Double{
        
        let sortedNumbers = sortNumbers()
        
        var numberToRound = result
        numberToRound.round()
        
        if sortedNumbers.count != 1 {
            
            if floor(result) == result {
                        
                print("Rule 1")
                return sortedNumbers[Int(result) - 1]
                        
            } else if floor(result) != result && !String(result).contains(".5") {
                        
                print("Rule 2")
                return sortedNumbers[Int(result.rounded() - 1)]
                        
            } else {
                
                print("Rule 3")
                print(sortedNumbers[Int(floor(result) - 1)])
                print(sortedNumbers[Int(floor(result))])
                return (sortedNumbers[Int(floor(result) - 1)] + sortedNumbers[Int(floor(result))]) / 2
                        
            }
            
        } else {
            
            return sortedNumbers[0]
            
        }
        
        
        
        
    }
    
    private func sortNumbers () -> [Double] {
        
        return numbers.sorted(by: <)
        
    }
    

    
}
