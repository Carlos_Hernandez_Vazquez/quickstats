//
//  TextFieldExt.swift
//  Quick Stats
//
//  Created by mac on 09/03/21.
//

import UIKit

extension UITextField {
    
    func toggleMinus() {
        
            guard let text = self.text, !text.isEmpty else { return }
        
            self.text = String(text.hasPrefix("-") ? text.dropFirst() : "-\(text)")
        
        }
    
}
