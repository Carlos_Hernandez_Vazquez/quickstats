//
//  DateExt.swift
//  Quick Stats
//
//  Created by mac on 19/03/21.
//

import Foundation

extension Date {
    
 
    func addFormat () -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        return dateFormatter.string(from: self)
        
    }
    
    
}
