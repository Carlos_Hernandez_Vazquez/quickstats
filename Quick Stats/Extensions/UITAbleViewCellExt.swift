//
//  UITAbleViewCellExt.swift
//  Quick Stats
//
//  Created by mac on 20/03/21.
//

import UIKit

extension UITableViewCell {
    
    func configureCell (for cell : UITableViewCell, with number : Double) -> UITableViewCell {
        
        cell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.textLabel?.textColor = .white
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = String(describing: number)
        cell.textLabel?.font = UIFont(name: "Avenir Next Medium", size: 15)!
        
        return cell
        
    }
    
}
